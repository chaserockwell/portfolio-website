import { Contentful } from './contentful';
import { nav } from './nav';
import { scroll } from './scroll';

(function ($, window, document) {
	"use strict";

	///// Variables /////
	let contentful = new Contentful();

	contentful.init();

	// Logic that depends on the DOM loading
	$(() => {
		nav.init(toggleMobileNavMenu, toggleDesktopNavMenu);
		scroll.init(toggleMobileNavMenu, toggleDesktopNavMenu);
	});

	// Toggle open class on drop down menu and hamburger icon
	function toggleMobileNavMenu() {
		$('.nav-drop-menu').toggleClass('open');
		$('#mobileHamburger').toggleClass('open');
	}

	function toggleDesktopNavMenu() {
		$('.desktop-nav').toggleClass('open');
		$('#desktopHamburger').toggleClass('open');
	}
})(window.jQuery, window, document);