export const scroll = {
	pageTitle: 'landing',
	portfolioInfo: {
		first: {
			title: 'First Test Work',
			description: 'lorem ipsum dolor sit amet blll;a l dkjl ea; fea dl eia; dklsjf . djfoiea.',
			link: 'https://google.com'
		},
		second: {
			title: 'Second Test Work',
			description: 'Thsdl e;ailfdj . sdkfjeoa d,fja eoaf.d f.dakjf d.akfd oea.df kadjfoe.d',
			link: 'https://reddit.com'
		},
		third: {
			title: 'Third Test Work',
			description: 'lorem ipsum dolor sit amet blll;a l dkjl ea; fea dl eia; dklsjf . djfoiea.',
			link: 'https://mlssoccer.com'
		}
	},
	init(toggleMobileNavMenu, toggleDesktopNavMenu) {
		// Window scroll events
		$(window).scroll(() => {
			this.handlePageScrollEventsThrottled();
		});

		// Scroll event for anchor tags
		$('a[href^="#"]').on('click', function (event) {
			try {
				let index = this.href.indexOf('#'),
					target = $(this.href.substring(index, this.href.length));

				if (target.length) {
					event.preventDefault();
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					if ($('#mobileHamburger').hasClass('open')) {
						toggleMobileNavMenu();
					}
					if ($('#desktopHamburger').hasClass('open')) {
						toggleDesktopNavMenu();
					}
				}
			} catch (error) {
				console.error(error);
			}
		});
	},
	handlePageScrollEvents() {
		let scrollInfo = {
			scrollPos: scrollY || pageYOffset,
			windowHeight: $(window).height(),
			navHeight: $('#nav').height()
		};
		scrollInfo.buffer = scrollInfo.navHeight + 100;
		this.setPageTitle(scrollInfo);
		this.setMobileNavColor(scrollInfo);
		this.setPortfolioInfo(scrollInfo);
	},
	// Change the page title when page is scrolled to each anchor
	setPageTitle(scrollInfo) {
		// Set window and section scroll positions and other variables
		let landingPos = $('#landing').offset().top - scrollInfo.buffer,
			aboutPos = $('#about').offset().top - scrollInfo.buffer,
			contactPos = $('#contact').offset().top - scrollInfo.buffer,
			portfolioPos = $('#portfolio').offset().top - scrollInfo.buffer,
			isTitleChanged = false;

		if (this.pageTitle !== 'landing' && scrollInfo.scrollPos > landingPos && scrollInfo.scrollPos < aboutPos) {
			this.pageTitle = 'landing';
			isTitleChanged = true;
		} else if (this.pageTitle !== 'about' && scrollInfo.scrollPos > aboutPos && scrollInfo.scrollPos < portfolioPos) {
			this.pageTitle = 'about';
			isTitleChanged = true;
			$('.work-info-container').removeClass('fixed');
		} else if (this.pageTitle !== 'portfolio' && scrollInfo.scrollPos > portfolioPos && scrollInfo.scrollPos < contactPos) {
			this.pageTitle = 'portfolio';
			isTitleChanged = true;
			$('.work-info-container').addClass('fixed');
		} else if (this.pageTitle !== 'contact' && scrollInfo.scrollPos > contactPos) {
			this.pageTitle = 'contact';
			isTitleChanged = true;
			$('.work-info-container').removeClass('fixed');
		}

		if (isTitleChanged) this.changePageTitle();
	},
	// Change pageTitle UI
	changePageTitle() {
		$('.page-title.active').removeClass('active');
		$('div.nav-item.selected').removeClass('selected');
		$('.page-title.' + this.pageTitle).addClass('active');
		$('a.' + this.pageTitle + '-a div.nav-item').addClass('selected');
	},
	setMobileNavColor(scrollInfo) {
		let groundHeight = $('#ground').height();
		let groundPos = $('#ground').offset().top - 100 + groundHeight / 2;

		if (scrollInfo.scrollPos > groundPos) {
			$('.mobile-nav').addClass('white');
		} else {
			$('.mobile-nav').removeClass('white');
		}
	},
	// Set the portfolio info on scroll
	setPortfolioInfo(scrollInfo) {
		let first = $('#firstWork').offset().top - scrollInfo.buffer,
			second = $('#secondWork').offset().top - scrollInfo.buffer,
			third = $('#thirdWork').offset().top - scrollInfo.buffer,
			moreWork = $('#moreWork').offset().top - scrollInfo.buffer - 50,
			isPortfolioInfoChanged = false;
		if (this.pageTitle === 'portfolio') {
			if (scrollInfo.scrollPos > first && scrollInfo.scrollPos < second) {
				$('#workTitle').html(this.portfolioInfo.first.title);
				$('#workDescription').html(this.portfolioInfo.first.description);
				this.setWorkInfoColor('yellow');
				$('#workInfo').show();
			} else if (scrollInfo.scrollPos > second && scrollInfo.scrollPos < third) {
				$('#workTitle').html(this.portfolioInfo.second.title);
				$('#workDescription').html(this.portfolioInfo.second.description);
				this.setWorkInfoColor('black');
			} else if (scrollInfo.scrollPos > third && scrollInfo.scrollPos < moreWork) {
				$('#workTitle').html(this.portfolioInfo.third.title);
				$('#workDescription').html(this.portfolioInfo.third.description);
				this.setWorkInfoColor('white');
				$('#workInfo').show();
			} else if (scrollInfo.scrollPos > moreWork) {
				$('#workInfo').hide();
			}
		}
	},
	setWorkInfoColor(color) {
		$('#workInfo').removeClass('yellow black white');
		$('#workInfo').addClass(color);
	},
	handlePageScrollEventsThrottled() {
		_.throttle(this.handlePageScrollEvents, 150).call(this);
	}
};
