export const nav = {
	init(toggleMobileNavMenu, toggleDesktopNavMenu) {
		// Trigger hamburger icon events
		$('#mobileHamburger').click(() => {
			toggleMobileNavMenu();
		});

		$('#desktopHamburger').click(() => {
			toggleDesktopNavMenu();
		});
	}
};