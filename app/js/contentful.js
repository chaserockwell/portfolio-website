export class Contentful {
	constructor() {
		this.accessToken = '0a4fd82b7216d08d316fa13a129710703f01271f7fd704330573f8caa1664e6a';
		this.client = undefined;
		this.space = 'lyxn45wtw6dm';
	}

	init() {
		this.client = window.contentful.createClient({
			accessToken: this.accessToken,
			space: this.space
		});
	}

	getEntries(options) {
		return this.client.getEntries(options);
	}

	getFeaturedPortfolioPieces() {
		const options = {
			'content_type': 'portfolio-piece',
			'fields.featured': true
		};
		return this.getEntries(options);
	}

	getPaginatedPortfolioPieces(page, limit) {
		const options = {
			'content_type': 'portfolio-piece',
			'skip': page * 10,
			'limit': (page + 1) * 10
		};
		return this.getEntries(options);
	}
}