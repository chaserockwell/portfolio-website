(function () {
	"use strict";

	// Load plugins
	var gulp = require('gulp'),
			babelify = require('babelify'),
			merge = require('gulp-merge'),
			fs = require('fs'),
			sequence = require('gulp-sequence'),
			sass = require('gulp-sass'),
			pug = require('gulp-pug'),
			autoprefixer = require('gulp-autoprefixer'),
			cssnano = require('gulp-cssnano'),
			uglify = require('gulp-uglify'),
			imagemin = require('gulp-imagemin'),
			rename = require('gulp-rename'),
			concat = require('gulp-concat'),
			browserSync = require('browser-sync').create(),
			browserify = require('browserify'),
			sourcemaps = require('gulp-sourcemaps'),
			source = require('vinyl-source-stream'),
			buffer = require('vinyl-buffer'),
			del = require('del');

	// Templates
	gulp.task('templates', function () {
		return merge(
			gulp.src('app/index.pug')
				.pipe(pug())
				.pipe(gulp.dest('dist')),
			gulp.src('app/**/*.pug')
				.pipe(pug())
				.pipe(gulp.dest('dist/html'))
		);
	});

	// Styles
	gulp.task('styles', function () {
		return gulp.src('app/sass/main.scss')
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer('last 2 version'))
			.pipe(gulp.dest('dist/css'))
			.pipe(rename({ suffix: '.min' }))
			.pipe(cssnano())
			.pipe(gulp.dest('dist/css'))
			.pipe(browserSync.stream());
	});

	gulp.task('style-watch', ['styles'], function(done) {
		browserSync.reload();
		done();
	});

	// Scripts
	gulp.task('scripts', function () {
		var bundler = browserify('app/js/main.js');
		bundler.transform(babelify);

		bundler.bundle()
			.on('error', error => {
				console.error(error);
			})
			.pipe(source('main.js'))
			.pipe(buffer())
			.pipe(sourcemaps.init({ loadMaps: true }))
			.pipe(rename({ suffix: '.min' }))
			.pipe(uglify())
			.pipe(gulp.dest('dist/js/'));
	});

	gulp.task('js-watch', ['scripts'], function (done) {
			browserSync.reload();
			done();
	});

	// Images
	gulp.task('images', function () {
		return gulp.src('app/images/**/*')
			.pipe(imagemin({optimizationLevel: 5, progressive: true, interlaced: true}))
			.pipe(gulp.dest('dist/img'));
	});

	// Third-party
	gulp.task('vendors', function() {
		var libs = ['node_modules/jquery/dist/jquery.min.js', 'node_modules/lodash/lodash.min.js'];

		return merge(
			gulp.src('app/vendor/*.css')
				.pipe(concat('vendor.bundle.css'))
				.pipe(cssnano())
				.pipe(gulp.dest('dist/vendor')),
			gulp.src(libs)
				.pipe(concat('third-party.bundle.js'))
				.pipe(gulp.dest('dist/vendor'))
		);
	});

	gulp.task('browser-sync', function() {
		browserSync.init({
			server: {
				baseDir: "./dist"
			}
		});

		// Watch pug files
		gulp.watch('app/**/*.pug', ['templates'], function() {
			browserSync.stream();
		});

		// Watch .scss files
		gulp.watch('app/sass/**/*.scss', ['style-watch']);

		// Watch .js files
		gulp.watch('app/js/**/*.js', ['js-watch']);

		// Watch image files
		gulp.watch('app/images/**/*', ['images'], function() {
			browserSync.stream();
		});

		// Watch any files in dist/, reload on change
		// gulp.watch(['dist/**']).on('change', function() {console.log('hey'); browserSync.stream() });
	});

	// Clean
	gulp.task('clean', function () {
		return del(['dist/html', 'dist/css', 'dist/js', 'dist/img']);
	});

	// Default task
	gulp.task('default', sequence('clean', 'images', ['templates', 'styles', 'scripts', 'vendors', 'browser-sync']));
})();
